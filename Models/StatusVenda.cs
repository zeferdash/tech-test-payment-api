namespace tech_test_payment_api.Models
{
    public enum StatusVenda
    {
        AguardandoPagamento,
        PagamentoAprovado,
        Cancelada,
        EnviadoParaTransportadora,
        Entregue
    }
}