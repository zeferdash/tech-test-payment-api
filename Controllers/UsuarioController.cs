using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsuarioController : ControllerBase
    {
        private readonly PaymentContext _context;

        public UsuarioController(PaymentContext context){
            _context = context;
        }

        [HttpPost]
        public IActionResult Criar(Usuario usuario){
            _context.Add(usuario);
            _context.SaveChanges();
            
            return Ok(usuario);
        }

    }
}